# Theory

# Purpose

Read a JSON configuration file and build a webserver serving data to be classified ((website). Server will have prebuilt templates to render the various classification tasks cleanly, and allow clients to choose actions which are recorded by the server. All choices are saved in a sqlite database.

Templates should be user configurable.  Doesn't mean I have to have that done now, but I should choose a technology stack and design that makes this possible.

An important feature is the ability to present multiple inputs to the user if needed.  For example, an audio clip and a picture (different modalities of the same event), or a picture of depth and an rgb picture.  These items need to be trained together, so it would be helpful if they got labeled together (very important if there is so much data, that it all can't be labeled).


## Terminology:
  - Data: The collection of items (Photo/Text/Video/Audio) we need labeled
  - Datum: Single piece of Photo/Text/Video/Audio we need labeled
  - UID: Unique ID, a unique identifier per datum for an input source.
  - Class/Category: I use this interchangeably here. They denote a grouping of data.
  - Categorical Task: A human observers a datum and makes a classification.


### Validating Classes
In this task scenario we are verifying data that has already been labeled. There may be an unknown class that should be labeled. In this situation we can have a relabelling by the human if needed, or just a valid/invalid selection.

  - File Determination 
    - File/Directory Traversal
        - Determine if there are subdirectories, assume these mean categories
        - If there are no subdirectories, only files, assume these are the data and we need to determine categories an alternate way (see below)
    - Data File - CSV File
        - No need for directory traversal. All information is provided within the file
        - File path is provided, relative from directory variable provided
  - Unique ID Determination
      - File Name without extension
      - Regex - Provide a regex to parse file name
  - Class Determination
      - Directory Name
      - Regex - Provide regex to parse a file name
  - Class Names
      - Optional, will contain the full list of class names. Use this if there are classes not labeled in your data set.
  - Class Names Mapping
      - Just a mapping from parsed categories to a new category name
      - 0 -> Truck, 1 -> Car
          - Easier for a human to label. Needed if the file names/directories are using indexes
    

### Labeling Classes
In this task scenario we have all unknown data.

  - File/Directory Traversal 
      - Not need to do this, everything should be mixed together
  - Class Determination
      - No need, we dont know the classes
  - Class Names
      - A list of class names, these should be human presentable
  - Class Name Mapping
      - Not needed, we never parsed and indexed classes.


# JSON Configuration File

A JSON configuration file should be all that is needed to start up the server and begin serving the tasks. 

Ideas for the JSON file

```json
{
  "website_name": "Classification Website",
  "tasks": [
    {
      "name": "Roof Classification",
      "description": "This task will classify roof shapes using using aerial imagery and LIDAR data.",
      "type": "categorical",
      "prelabeled": true,
      "inputs": [
        {
          "name": "Aerial Image",
          "directory": "D:\\Research\\roof_learning\\witten\\pics\\train\\rgb",
          "unique_id_regex": "(.*)_.*_.*",
          "widget": "picture"
        },
        {
          "name": "LIDAR Image",
          "directory": "D:\\Research\\roof_learning\\witten\\pics\\train\\lidar",
          "unique_id_regex": "(.*)_.*_.*",
          "widget": "picture"
        }
      ]
    }
  ]
}
```

### Data Storage

Here we will discuss what data is stored in the SQlite database. After the configuration file is parsed and validated, the program will know where the data needing human work resides.  It will then create a database table for each task input and populates it with the rows (datum) that need to be created. It determines the items either by walking through the directory or using an input file (explained above).  The schema will look as so.

TODO: Update Table Schemas; Task, Inputs, UserLog

| Column Name | Data Type | Description                                                             |
|-------------|-----------|-------------------------------------------------------------------------|
| uid         | number    | Primary Key, File Name or a regex that extracts the id                  |
| src         | string    | Full File Path                                                          |
| url         | string    | URL path to the static file served from webserver                       |
| category    | string    | String indicating the class                                             |
| verified    | int       | Number of times file has been verified by a human                       |
| issue       | bool      | A flag raised by a human to indicate there is an issue (Default False)  |

A couple issues with this schema:
1. Only one person (or at least the last person) can update the class/verification process
    1. We can actually just keep a generic user interaction log to help with this though.


Things not stored
  * User ID's or passwords for login.  This is only in the configuration file and loaded into memory.
  * Templates to be used to render.  This is only in the configuration file and loaded into memory.


## Client Side

### Option 1

Client side should be server side rendered using the built in flask Jinja template engine.  This allows minimal dependencies and sticks to one language needed (nodejs not needed).  It also allows others to construct custom templates easier.

### Option 2 (I want this, but putting on back burner for simplicity)

Client side should be a simple pleasant user interface. It will be implemented as a Single Page Application (SPA), and server information will be transferred
through an API on the route `/api`.

The pages are as follows:
- Home page
    - A little info about the website and then a board of the available tasks.
- Task Page
    - Open up a task and immediately begins serving the content.
    - Help Screen or Modal pop up to help with the task



How I see the flow of interaction
1. User Navigates to the base URL (e.g. http://example.com/). If no data authorization is present, then the client side router routes to a login page (e.g. http://example.com/#/login)
2. Login form is presented. User logins with ajax. Credentials saved in a global object.  Client side rerotues to tasks ((e.g. http://example.com/tasks))
3. Board of tasks are presented. Dynamic routes are created and their templates for each task.  Client clicks on a task and is rerotued (e.g. http://example.com/)tasks/roof-learning)
4. Task is loaded and data is asychronously requested to load the page.



## Other

Have a queue in a database that fetches 1000 uid, that are either distributed uniformly for classes if prelabeled, or random if not

When we need something we server from the queue and pull from it, if the queue gets low we replenish it.

Nothing leaves the queue until a certain amount of unqique visits are given to it (default 1)

Whe we pull from the que for a user, we just make sure the user hasn't already visited the 


TWO TABLE TASK TABLE
INPUT TABLE

TASK IS JOINED WITH INPUTS on UID of INPUT table

Eveyr task has a status field (0, no one checked out, 1 person check out, 2 person checked out)
Thisi s updated any time a person checks an item out

IN MEMORY QUEUE is created for each user which pulls from the task table and then the also automaticlly updates the status of the items pulled.  A function requests a task from the quee, which pops from the queue and updates the quue when it gets too low.

When I pull from tasks for a user, how do I ensure that I dont pull a task that I have already looked out.  I need to join with the user log..





