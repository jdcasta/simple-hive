# Simple Hive

## Overview 

This repository contains code to generate and serve a website that constructs tasks that can be completed by humans. These tasks are mostly geared to labelling or validating a data set for machine learning algorithms.

Such tasks include:

* Classification of Images
* Classification of Audio (WIP)
* Classification of Text (WIP)

Some important highlights:

* Website is built from a very minimal configuration file (JSON format) which specifies project meta data (img/file locations, website name, etc.)
* User input/interactions are saved in a [sqlite database](https://en.wikipedia.org/wiki/SQLite)
* Very little dependencies (Python and few external libraries which are easily installed)
* Written in Python (95%) and Javascript (5%)
* Responsive website, works just fine on mobile.

For more detailed overview of the implementation/design, please read `theory.md`

Animated GIF of simple-hive in action:
![Example Simple Hive Site](pics/simple-hive-animal.gif "Example Simple Hive Site")

### Scope

There are other websites/services that do similar things.  Amazon's [Mechanical Turk](https://www.mturk.com/mturk/welcome) and [PyBossa](http://pybossa.com/) are the major ones I saw.  However I decided to make simple-hive for the following reasons:

* Need a service that is free, or at least cheaper than Mechanical Turks offering. Many times we have employed/volunteer students at a university who are yearning to contribute to projects.  Simple-hive makes that possible fairly easy.
* I needed something simple and small, that is not difficult to set up. I could not even get PyBossa set up to work (tried for 2 hours)
* I need the ability to manipulate the templates (HTML to user) for different data types.
* Needed Multi-Input tasks (e.g. multiple pictures for same task, or modalities)
* I DONT need a full blown webserver that serves thousands of people. I need a server that has on average less than 10 concurrent people using it.
* I DONT need a service to find people to perform the work for me, I can handle that myself.
* I need something drop dead simple.  A simple configuration file should be all thats necessary to set up the website. Set up should be painless.

For these reasons, simple-hive was created with the following thoughts in mind:

* Write it in python.  I wanted to write in Javascript for Node.js but I know most researchers are not comfortable with that.
* Give the researcher the ability to create new templates fairly easy.
* The installation needs to be painless.  Many people already have python, and pip is likely (hopefully) already installed to handle the *few* dependencies `simple-hive` has.
* Build everything from a simple configuration file.



## Requirements (Dependencies)

I tried hard to keep the dependencies very minimal.

1. flask (webserver)
2. python-slugify (converts arbitrary stings to url friendly string)
3. Flask-HTTPAuth (does basic http authentication)

Install all with the following command

* `pip install -r requirements.txt`

## Setup

Simply create a small configuration file (see examples in `server/tests/configs`) and `simple-hive` will generate a simple website for you.

### Examples

You can see try the following examples

#### Example 1

This example uses a data file to inform the `simple-hive` what items to show for each task

`python simple-hive.py --file server/tests/configs/single_input_data_file.json`


#### Example 2

This example automatically traverses a directory structure to determine which items to show for a task.

`python simple-hive.py --file server/tests/configs/single_input_prelabeled.json`

## Accessing Your Data

All data is stored in a SQLite database file.  I recommend downloading and using [SQLite Browser](http://sqlitebrowser.org/) to view and explore your data. It is very simple, cross-platform program (Windows, Mac, Linux), small (~15 MB), and even allows you to export your data to a CSV file.


The user interaction data is stored in a table called `user_log` within your sqlite database.  This schema has the following column:

* user (the user name)
* ts (time stamp)
* taskid (the index of your task in your config.json file)
* uid (unique id for this datum )
* category (what the category was labeled as)
* issue (weather the user flagges this datum with an issue)
* value3 (extra column placeholder for possibly more logging)

Example snapshot using SQLite Browser:

![Example User Log](pics/sqlite-browser-example.png "Example User Log")

There are also other tables created to keep to keep track of all the data to be classified and served to the client.


## Security and Creating a Public Facing Website

Very basic security is employed on this website.  In your `config.json` file, add a `users` key that holds an array of key/value pairs of username: password.


```json
{
  "website_name": "Classification Website",
  "users": {
    "admin": "password"
  }
```

 Please do not user any user/password values that are used anywhere else (gmail, outlook, etc.). The authentication uses [HTTP Basic Auth](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication) and is NOT secure *without* HTTPS.  To make the website public facing *with* HTTPS, you can use a service like [ngrok](https://ngrok.com/) which will tunnel your local ip to a secure public facing url. In other words something like this happens:

 ```
 Forwarding                    https://2b2369f9.ngrok.io -> localhost:5000
 ```

 and users can now access the website at the url `https://2b2369f9.ngrok.io`, which connects them to your local machine.





