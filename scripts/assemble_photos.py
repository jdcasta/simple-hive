" Will read a user_log table and assemble all valid pictures in destination directory "

import os
import argparse
import sqlite3
from shutil import copy

USER_LOG_QRY = """
SELECT *
FROM user_log
WHERE taskid = ? AND issue = 0
"""

INPUT_QRY = """
SELECT *
FROM {}
ORDER BY uid, inputid
"""

def dict_factory(cursor, row):
    " Converts SQL rows to dict "
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def make_dirs(base_dir, folder, dir_names):
    " Creates class subdirectories inside the given directory(s)"
    if isinstance(folder, str):
        directories = [folder]
    else:
        directories = folder
    for directory in directories:
        for class_dir in dir_names:
            new_class_dir = os.path.join(base_dir, directory, class_dir)
            if not os.path.exists(new_class_dir):
                os.makedirs(new_class_dir)

def parse_args():
    " Arg parsing function"
    argparser = argparse.ArgumentParser()
    # other arguments
    argparser.add_argument('--db', type=str, help="File path to the database")
    argparser.add_argument('--task_name', type=str, help="The task name to work on")
    argparser.add_argument('--task_index', type=str, help="The task index")
    argparser.add_argument('--dest', type=str, help="Destination of copied pictures")
    args = argparser.parse_args()
    return args

def create_db_connection(db_path):
    " Creates the DB connection "
    db_connection = sqlite3.connect(db_path, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
    db_connection.row_factory = dict_factory
    return db_connection

def query(db_conn, qry, params=None):
    " Executes the query and fetches all documents "
    curs = db_conn.cursor()
    if params:
        result = curs.execute(qry, params)
    else:
        result = curs.execute(qry)
    return result.fetchall()

def main():
    " Main Function"
    args = parse_args()
    db_conn = create_db_connection(args.db)

    # All logged user actions for specific task
    user_log = query(db_conn, USER_LOG_QRY, (args.task_index,))
    # Get all the input sources for this task
    input_sources = query(db_conn, INPUT_QRY.format(args.task_name + '_inputs'))

    # Get classes and directories
    classes = list(set([log['category'] for log in user_log]))
    input_types = list(set([input_source['name'] for input_source in input_sources]))
    # Make the input type directories as well as the class directories
    make_dirs(args.dest, input_types, classes)
    # Iterate through each unique id
    for log in user_log:
        # get the inputs for this uid
        inputs = list(filter(lambda input_source: input_source['uid'] == log['uid'], input_sources))
        for _input in inputs:
            # copy this src for this input over to the designated area
            copy(_input['src'], os.path.join(args.dest, _input['name'], log['category']))



if __name__ == '__main__':
    main()
