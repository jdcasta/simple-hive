SELECT *
FROM traffic_identification a
WHERE a.rowid IN (
    SELECT b.rowid
    FROM traffic_identification b 
    WHERE b.checkout < 1 
      AND b.rowid IS NOT NULL 
      AND a.category = b.category
    LIMIT 2
)
ORDER BY a.category, a.uid;