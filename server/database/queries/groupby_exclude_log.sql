BEGIN TRANSACTION;

SELECT t1.uid, category, verified, checkout, issue
FROM roof_classification t1
LEFT JOIN user_log t2 ON t2.uid = t1.uid AND t2.taskid = 0
WHERE t2.uid IS NULL and t1.checkout < 3
GROUP BY category
order by t1.uid;

UPDATE roof_classification
SET checkout= checkout + 1
WHERE rowid in (
SELECT t1.rowid
FROM roof_classification t1
LEFT JOIN user_log t2 ON t2.uid = t1.uid AND t2.taskid = 0
WHERE t2.uid IS NULL and t1.checkout < 3
GROUP BY category
order by t1.uid
);


COMMIT;