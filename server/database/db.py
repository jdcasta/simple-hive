"""
Handles DB connections and holds the queries
"""
import sys
from os import path
import threading
import re
import sqlite3
import logging
from datetime import datetime
# from itertools import izip

from server import config

DATABASE_DIR = path.dirname(path.abspath(__file__))


#### BEGIN TABLE SCHEMAS - See models.py for explanation ####
CREATE_USER_LOG_TABLE = """
CREATE TABLE IF NOT EXISTS %s (
    user STRING,
    ts TIMESTAMP,
    taskid INTEGER,
    uid INTEGER,
    category STRING,
    issue INTEGER,
    value3 STRING  
)
"""

CREATE_CATEGORICAL_TABLE = """
CREATE TABLE IF NOT EXISTS %s (
    uid INTEGER PRIMARY KEY ASC,
    category STRING,
    verified INTEGER,
    checkout INTEGER,
    issue INTEGER  
)
"""

CREATE_INPUTS_TABLE = """
CREATE TABLE IF NOT EXISTS %s (
    uid INTEGER,
    name STRING,
    inputid INTEGER,
    src STRING,
    url STRING,
    PRIMARY KEY (uid, inputid),
    FOREIGN KEY(uid) REFERENCES %s(uid)
)

"""

GET_NEXT_GROUP_PRELABELED = """
SELECT t1.uid, t1.category, t1.verified, t1.checkout, t1.issue
FROM {0} t1
LEFT JOIN {1} t2 ON t2.uid = t1.uid AND t2.taskid = ? AND t2.user = ?
WHERE t2.uid IS NULL AND t1.verified < ? AND t1.checkout < ? 
GROUP BY t1.category
ORDER by t1.uid
"""

GET_NEXT_GROUP_UNLABELED = """
SELECT t1.uid, t1.category, t1.verified, t1.checkout, t1.issue
FROM {0} t1
LEFT JOIN {1} t2 ON t2.uid = t1.uid AND t2.taskid = ? and t2.user = ?
WHERE t2.uid IS NULL and t1.verified < ? AND t1.checkout < ?
order by t1.uid
LIMIT 10
"""

UPDATE_CHECKOUT_GROUP = """
UPDATE {}
SET checkout = checkout + 1
WHERE rowid in ({})
"""

GET_INPUTS = """
SELECT * 
FROM %s
WHERE uid IN (%s)
ORDER BY uid, inputid
"""

INSERT_INTO_CATEGORICAL_TABLE = """
INSERT OR IGNORE INTO %s (uid, category, verified, checkout, issue) VALUES (?,?,?,?,?)
"""

INSERT_INTO_INPUT_TABLE = """
INSERT OR IGNORE INTO %s (uid, name, inputid, src, url) VALUES (?,?,?,?,?)
"""

INSERT_INTO_USER_LOG_TABLE = """
INSERT OR IGNORE INTO user_log (user, ts, taskid, uid, category, issue, value3) VALUES (?,?,?,?,?,?,?)
"""

UPDATE_CATEGORICAL_ITEM = """
UPDATE %s
SET verified = verified + 1
WHERE uid = ?
"""


class LockableCursor:
    def __init__ (self, cursor):
        self.cursor = cursor
        self.lock = threading.Lock ()

    @property
    def rowcount(self):
        return self.cursor.rowcount

    def execute (self, query, params=None, auto_release=True, fetch_type=None):
        self.lock.acquire()
        try:
            if params: 
                result = self.cursor.execute(query, params)
            else:
                result = self.cursor.execute(query)
            if fetch_type == 'all':
                return result.fetchall()
            if fetch_type == 'one':
                return result.fetchone()
            else:
                return result
        except Exception as exception:
            raise exception
        finally:
            if auto_release:
                self.lock.release()

class MyDB(object):
    """
    DB Connection Object
    """
    _db_connection = None
    _db_cur = None

    def __init__(self, db_path):
        self.table = {}
        try:
            self._db_connection = sqlite3.connect(
                db_path, check_same_thread=False, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            self._db_connection.row_factory = sqlite3.Row
            self._db_cur = LockableCursor(self._db_connection.cursor())
        except Exception as error:
            logging.error(
                "Could not open up SQLite file: %s. Error: %r", db_path, error)
            sys.exit('Exiting...')

    def query(self, query, params=None, auto_release=True, fetch_type=None):
        """
        Executes a query, and returns the result
        """
        if params:
            return self._db_cur.execute(query, params, auto_release=auto_release, fetch_type=fetch_type)
        else:
            return self._db_cur.execute(query, auto_release=auto_release, fetch_type=fetch_type)

    def execute_queries(self, queries):
        """
        Executed multiple queries with a transaction
        """
        self._db_cur.execute('BEGIN TRANSACTION')
        for (query, params) in queries:
            self.query(query, params)
        self._db_cur.execute('COMMIT')
        return self._db_cur.rowcount

    def create_table(self, tablename, schema='categorical'):
        self.table[tablename] = schema
        if schema == config.CATEGORICAL:
            query = (CREATE_CATEGORICAL_TABLE % (tablename))
        elif schema == config.USER_LOG:
            query = (CREATE_USER_LOG_TABLE % (tablename))
        elif schema == config.INPUT:
            foreign_table = tablename.rsplit('_', 1)[0]
            query = (CREATE_INPUTS_TABLE % (tablename, foreign_table))
        else:
            logging.error('table name has no supported schema, %s', tablename)
            sys.exit('Exiting...')
        self.query(query)
        self._db_connection.commit()

    @staticmethod
    def chunks(data, rows=10000):
        """ Divides the data into 1000 rows each """
        for i in range(0, len(data), rows):
            yield data[i:i + rows]

    def insert_batch(self, tablename, items):
        """
        We could be inserting hundreds of thousands of records. Instead of inserting one by one,
        we will break up the data into chunks of size 1000.  We then performa  database transaction
        insert all 1000 at the same time
        """
        query = ''
        if self.table[tablename] == config.CATEGORICAL:
            query = INSERT_INTO_CATEGORICAL_TABLE
        elif self.table[tablename] == config.INPUT:
            query = INSERT_INTO_INPUT_TABLE
        else:
            logging.error('table name has no supported schema, %s', tablename)
            sys.exit('Exiting...')
        # insert the table name into the query
        query = query % (tablename)

        chunk_generator = self.chunks(items)
        for chunk in chunk_generator:
            self._db_cur.execute('BEGIN TRANSACTION')
            # TODO: Make this independent of model, attribute lookup?
            for model in chunk:
                if self.table[tablename] == config.CATEGORICAL:
                    params = (model.uid, model.category,
                              model.verified, model.checkout, model.issue)
                elif self.table[tablename] == config.INPUT:
                    params = (model.uid, model.name,
                              model.inputid, model.src, model.url)

                self.query(query, params)

            self._db_cur.execute('COMMIT')

    def __del__(self):
        self._db_connection.close()

# Helper Methods
def create_db():
    """
    Creates a database connection and stores it in the module config for future use
    """
    db_path = path.join(DATABASE_DIR, config.CONFIG['db_name'])
    if path.exists(db_path):
        logging.info(
            "DB file already exists. Will reuse existing DB. This message can appear in multithreading environment even with creation of new database.")
    config.DB_CONN = MyDB(db_path)


def update_task_item(task_item, user='unknown'):
    """
    task_id, uid, category, issue. Only works for categorical task for right now
    """
    task = config.CONFIG['tasks'][task_item['taskid']]
    try:
        table = task['tables'][0]
        query = UPDATE_CATEGORICAL_ITEM % (table)
        params = (task_item['uid'], )

        config.DB_CONN.query(query, params)
        # log the users action into the database
        add_log(user, task_item['taskid'], task_item['uid'],
                task_item['category'], task_item['issue'])

        return 1
    except Exception as error:
        logging.error("Could not commit this task. Error: %r", error)
        return 0


def add_log(user, taskid, uid, category, issue='', value3=''):
    config.DB_CONN.query(INSERT_INTO_USER_LOG_TABLE, (user,
                                                      datetime.now(), taskid, uid, category, issue, value3), auto_release=False)
    config.DB_CONN._db_connection.commit()
    config.DB_CONN._db_cur.lock.release()



def fetch_task_item(username, task):
    # first in list is the task table, second in the list is the input table
    task_table_name = task['tables'][0]
    user_queue = config.USER_QUEUE.get(username)
    if user_queue is None:
        # First time this user has made a request
        user_queue = config.USER_QUEUE[username] = {task_table_name: []}
    # Get the task queue for this specific user
    task_user_queue = user_queue.get(task_table_name)
    if task_user_queue is None or len(task_user_queue) == 0:
        # We dont have any data in the queue, we need to make a database call to fetch more
        # gets a batch of items from the task table
        task_items = get_task_items_for_task(username, task)
        # gets a batch of inputs for the corresponding tasks fetched above
        task_item_inputs = get_inputs_for_task(task, task_items)
        # print(task_items)
        task_item_inputs = [task_item_inputs[i:i + len(task['inputs'])] for i in range(
            0, len(task_item_inputs), len(task['inputs']))]
        # print(task_item_inputs)
        for task_item, inputs in zip(task_items, task_item_inputs):
            # print(task_item, inputs)
            task_item['inputs'] = inputs
        # print(task_items)
        task_user_queue = config.USER_QUEUE[username][task_table_name] = task_items
        if not task_user_queue:
            return None

    # We have enough data in the queue now, so just pop off the last one
    return task_user_queue.pop()


def get_task_items_for_task(username, task):
    " Fetches a group of task_items form the given task for a user, increments their checkout column "
    # first in list is the task table, second in the list is the input table
    task_table_name = task['tables'][0]
    if task['prelabeled']:
        query = GET_NEXT_GROUP_PRELABELED.format(
            task_table_name, config.USER_LOG)
    else:
        query = GET_NEXT_GROUP_UNLABELED.format(
            task_table_name, config.USER_LOG)
    params = (task['id'], username,
              config.CONFIG['number_independent_validations'], config.CONFIG['number_independent_validations'])
    task_items = config.DB_CONN.query(query, params, fetch_type='all')
    task_items = [dict(item) for item in task_items]

    # Here we will mark these task as having been checked out by a user
    # A simple regex that modifies the previous query to only return the rowid, we then update these rowid's checkout column in the query
    update_query = re.sub('SELECT.*FROM', 'SELECT t1.rowid FROM',
                          UPDATE_CHECKOUT_GROUP.format(task_table_name, query), flags=re.DOTALL)
    config.DB_CONN.query(update_query, params, auto_release=False)  # execute the update query
    config.DB_CONN._db_connection.commit()      # commit the changes
    config.DB_CONN._db_cur.lock.release()
    return task_items                           # return the task items


def get_inputs_for_task(task, task_items):
    " Gets inputs for each task_item in the list task_items "
    # first in list is the task table, second in the list is the input table
    input_table_name = task['tables'][1]
    uid_list = [item['uid'] for item in task_items]
    query = GET_INPUTS % (
        input_table_name, ', '.join('?' for unused in uid_list))
    input_list = config.DB_CONN.query(query, uid_list, fetch_type='all')
    input_list = [dict(item) for item in input_list]    # turn them into dicts
    return input_list

