from datetime import datetime
class Categorical(object):
    """
    Class to describe the structure of our categorical data to be stored in SQLite
    """

    def __init__(self, uid, category='', verified=0, issue=0, checkout=0):
        self.uid = uid # unique id
        self.category = category # class or category of item
        self.verified = verified # number of items the item has been seen by a human and verified
        self.checkout = checkout
        self.issue = issue # weather there is an  issue with the data, this is like a flag

    def __repr__(self):
        return '(%s, %s, %s, %s)' % (
            self.uid, self.category,
            self.verified, self.issue)


class Input(object):
    """
    Class to describe the structure of our categorical data to be stored in SQLite
    """

    def __init__(self, uid, name, inputid, src, url):
        self.uid = uid # unique id
        self.name = name
        self.inputid = inputid
        self.src = src # file src if applicable
        self.url = url # url to fetch the file

    def __repr__(self):
        return '(%s, %s, %s, %s)' % (
            self.uid, self.name,
            self.src, self.url)



class UserLog(object):
    """
    Class to describe the structure of our user log
    """

    def __init__(self, user, taskid, uid, category, ts=None, issue='', value3=''):
        self.ts = ts if ts else datetime.now() # time stamp
        self.user = user # user name
        self.taskid = taskid,
        self.uid = uid,
        self.category = category # Arbitrary value to log
        self.issue = issue # Arbitrary value to log
        self.value3 = value3 # Arbitrary value to log

    def __repr__(self):
        return '(%s, %s, %s, %s, %s, %s, %s)' % (
            self.ts, self.user, self.taskid, self.uid, self.category,
            self.issue, self.value3)

