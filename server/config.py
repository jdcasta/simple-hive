"""
Configuration File
Like a global variable accessed throughout this program
"""
DEFAULT_CONFIG_TOP = {
    "db_name": "hive.db",
    "website_name": "Classification Website",
    "users": {
        "admin": "password"
    },
    "number_independent_validations": 1
}

DEFAULT_TASK = {
    "name": "task",
    "description": "A simple task that needs your help",
    "classes": [],
    "type": "categorical",
    "prelabeled": False
}

DEFAULT_INPUT = {
    "name": "input",
    "data_file": None,
    "unique_id_regex": None,
    "class_type_regex": None,
    "valid_file_regex": None,
    "widget": "picture",
    "template": None,
    "class_map": None
}
# Task Types
CATEGORICAL = 'categorical'

# STRINGS USED THROUGHOUT PROGRAM
INPUT = 'input'
USER_LOG = 'user_log'

# WHITE LIST FILE EXTENSION FOR PICTURES
PICTURE_WHITE_LIST = ['jpg', 'jpeg', 'png', 'bmp']

DEFAULT_CONFIG_FILE_NAME = 'config.json'

# A list of directories for data files and their associated base URL handle for this webserver
STATIC_DIRS = []
# { slug: /static/task/roof-classification/input/aerial-image/, 'directory': "some/fully/specified/file/dir"

# Will hold the merged configuration DEFAULT_CONFIG and the users supplied configuration (config.json)
CONFIG = {}
# Will hold the database connection
DB_CONN = None

# Each user has a queue of tasks items to complete. We fetch items from the database (save them) and then serve them from this queue
USER_QUEUE = {

}
