"""
This module is responsible for proper parsing of the configuration file
config.json and the initial construction/population of the database
"""
import os
from os import path
import sys
import json
import logging
import re
import csv

from slugify import slugify

from server import config
from server.database import db
from server.database.models import Categorical, Input
from server.application import app


def scrub(table_name):
    """
    Cleans a user defined table name to be a valid sqlite table name
    """
    table_name = table_name.replace(" ", "_")
    return ''.join(chr for chr in table_name if chr.isalnum() or chr == '_').lower()


def set_index(config, index):
    """
    Just adds a number to end of a name
    """
    config['name'] = config['name'] + '_' + str(index)
    config['id'] = index
    return config


def merge_two_dicts(x, y):
    """
    Given two dicts, merge them into a new dict
    TODO: Fix this double update nonsense
    """
    temp = dict(x)
    temp.update(y)
    y.update(temp)
    return y


def apply_defaults(user_config):
    """
    Applies default settings to a user configuration file
    """
    user_config = merge_two_dicts(config.DEFAULT_CONFIG_TOP, user_config)
    for i, task in enumerate(user_config['tasks']):
        task = merge_two_dicts(set_index(config.DEFAULT_TASK, i), task)
        for j, _input in enumerate(task['inputs']):
            if not path.isabs(_input['directory']):
                _input['directory'] = path.join(os.getcwd(), _input['directory'])
            _input = merge_two_dicts(
                set_index(config.DEFAULT_INPUT, j), _input)
    return user_config


def validate(config):
    """ Will validate the dictionary """
    # TODO Implement this function
    return True


def parse_file(file):
    """
    Parses the user configuration file.  Results end up stored in the config module. Yucky global variables.
    """
    with open(file) as conffile:
        user_config = json.loads(conffile.read())
    # Merge the dictionaries, overriding the default
    user_config = apply_defaults(user_config)
    # print(user_config)
    if not validate(user_config):
        sys.exit('Invalid Configuration file....exiting')
        return None
    else:
        # Store the configuration in the global config module
        config.CONFIG = user_config


# def has_sub_dir(directory)

def get_immediate_subdirectories(directory):
    """
    Returns the immediate subdirectories ofa directory. Empty list if none
    """
    return [os.path.join(directory, name) for name in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, name))]


def list_valid_files_in_sub_dir(directory, white_list_formats, task, task_input, base_url, input_index=0, include_dir_in_slug=False):
    """
    Will go through a directory and list all the valid files
    Will parse the file name to produce a valid unique id by removing extension (or regex if provided)
    Will determine the class through through a regex if provided. Maps class name if a map is provided.
    Returns a list of Model Objects and Input Objects and classes
    """
    determine_class, unique_id_regex, class_type_regex, class_map = task['prelabeled'], task_input['unique_id_regex'], \
                                                                    task_input['class_type_regex'], task_input['class_map']
    categories = set() # unique list of categories
    task_items = []    # list of all task items to add
    input_items = []   # list of all inputs to add
    base_dir = path.basename(directory)
    for fname in os.listdir(directory):
        is_valid = False
        for extension in white_list_formats:
            if fname.lower().endswith('.' + extension):
                is_valid = True
                break
        if is_valid:
            logging.debug('Valid File: %s', fname)
            # Default uid and category names
            uid = fname.split('.')[0]  # default unique id
            category = base_dir if determine_class else ''
            # If uid must be created through user passed regex
            if unique_id_regex is not None:
                match = re.match(unique_id_regex, fname)
                if match and match.groups():
                    uid = int(match.groups()[0])
                else:
                    logging.error(
                        'UID Regex (%s) failed on file (%s)', unique_id_regex, fname)
                    continue
            # If class name must be determined through user passed regex, run on FILE name, not diretory
            if class_type_regex is not None:
                match = re.match(class_type_regex, fname)
                if match and match.groups():
                    category = match.groups()[0]
                else:
                    logging.error(
                        'Class Regex (%s) failed on file (%s)', unique_id_regex, fname)
                    continue

            # Source should 
            src = path.join(task_input['directory'], fname)

            # Create a url that is RELATIVE to the root of the website. urljoin requires the http scheme in the front, so we add it
            # and then remove it with urlparse and get only the path
            url = base_url + base_dir + '/' + fname if include_dir_in_slug else base_url + fname
            category = class_map[category] if class_map else category
            categories.add(category)
            # Get unique id
            task_item = Categorical(uid=uid, category=category)
            input_item = Input(uid, task_input['name'], input_index, src, url)

            task_items.append(task_item)
            input_items.append(input_item)

            logging.debug('New Item: %r', task_item)
    return task_items, input_items, categories


def get_white_list(task_input):
    """
    Returns white list of file extensions based on the widget asked for
    """
    if task_input['widget'] == 'picture':
        return config.PICTURE_WHITE_LIST


def generate_static_dir_slug(directory, taskname, inputname):
    return {
        'slug': '/static/task/{}/input/{}/'.format(slugify(taskname), slugify(inputname)),
        'directory': directory
    }


def get_categorical_items_from_data_file(task, task_input, base_url, input_index=0):
    """
    return the table items and categories
    """
    categories = set() # List of classes found
    task_items = []    # list of all task items to add
    input_items = []   # list of all inputs to add
    try:
        with open(task_input['data_file']) as file:
            data = csv.DictReader(file, delimiter=',')
            for row in data:
                # Get the category, default to emptystring
                category = row['category'] if task['prelabeled'] else ''
                # get the file name
                fname = path.basename(row['src'])
                # These if statements determine the unique id for this file
                if row.get('uid') is not None:
                    uid = int(row['uid'])
                elif task_input.get('unique_id_regex'):
                    # User provided a regex to parse the uid...
                    match = re.match(task_input['unique_id_regex'], fname)
                    if match and match.groups():
                        uid = int(match.groups()[0])
                    else:
                        logging.error('UID Regex (%s) failed on file (%s)',
                                      task_input['unique_id_regex'], fname)
                        continue
                else:
                    # Default UID is simply the file name without extension
                    uid = int(path.splitext(fname)[0])

                # the VARIABLE src will be the full file path
                src = path.join(task_input['directory'], row['src'])
                url = base_url + row['src'] # relative url
                # Map the category if user provides a class map
                category = task_input['class_map'][category] if task_input['class_map'] else category
                categories.add(category) # add category to set, only adds if unique

                # item = Categorical(uid=uid, src=src, url=url, category=category)

                task_item = Categorical(uid=uid, category=category)
                input_item = Input(uid, task_input['name'], input_index, src, url)

                task_items.append(task_item)
                input_items.append(input_item)

                # table_items.append(item)
            return task_items, input_items, categories
    except FileNotFoundError:
        logging.error('Could not open %s file', task_input['data_file'])
        sys.exit('Exiting...')
    except Exception as e:
        logging.error('Error parsing data file: %s. Error: %r ',
                      task_input['data_file'], e)
        sys.exit('Exiting...')


def build_categorical_tables(task):
    """
    This will take a task (which could have multiple input types (audio, video), but have same categorization task) and will create
    a DB table for the task and then another database table for the input.
    The task table holds information for a unique item (has uid) such as the category, how many times its been verified. Only one row
    for each uid.
    The input table hold information of the unique data item, like the url for a picture, or an audio file. Can have multiple rows for each uid
    if there are multiple input sources.
    """
    # A Key value Pair of table_name: array of items
    db_tables = {}
    categories = set() # unique list of all category/class names
    task_items = []     # list of task_items. Object Class is Category (usually)
    input_items = []    # list of input items. Object Class is Input

    task_table_name = scrub(task['name'])
    input_table_name = scrub(task_table_name + '_inputs')
    for input_index, task_input in enumerate(task['inputs']):
        # This adds a record of a static directory that will be served
        base_url = generate_static_dir_slug(task_input['directory'], task['name'], task_input['name'])
        config.STATIC_DIRS.append(base_url)
        # Determine if we are resolving file through directory traversal or through a provided data file
        if task_input['data_file']:
            # Get file source from data file
            task_items_new, input_items_new, categories_new = get_categorical_items_from_data_file(
                task, task_input, base_url['slug'], input_index=input_index)
            input_items = input_items + input_items_new # fast append
            if input_index == 0:
                # Task Items can be generated from just the first input, they only contain basic information like the uid. 
                task_items = task_items + task_items_new
            categories = categories.union(categories_new)
        else:
            # Perform Directory Traversal Here
            white_list_formats = get_white_list(task_input)
            # To determine the category we first see if there is a list of directories with class names, subfolder will be full if there are
            subfolders = get_immediate_subdirectories(
                task_input['directory'])

            # Classes are NOT organized by subfolder.  Therefore the only search this top level directory
            include_dir_in_slug = True
            if not subfolders:
                subfolders.append(task_input['directory'])
                include_dir_in_slug = False

            for subfolder in subfolders:
                task_items_new, input_items_new, categories_new = list_valid_files_in_sub_dir(
                        subfolder, white_list_formats, task, task_input, base_url['slug'], input_index=input_index, include_dir_in_slug=include_dir_in_slug)
                input_items = input_items + input_items_new # fast append
                if input_index == 0:
                    # Task Items can be generated from just the first input, they only contain basic information like the uid. 
                    task_items = task_items + task_items_new

                categories = categories.union(categories_new)

    db_tables = {
        task_table_name: task_items,
        input_table_name: input_items
    }
    # update the list of all possible classes for this task
    task['classes'] = list(categories.union(task['classes']))
    return db_tables


def construct_tables(tables):
    """
    Constructs tables if they dont exist
    """
    for table_name, items in tables.items():
        schema = items[0].__class__.__name__.lower()  # 'ie. categorical'
        config.DB_CONN.create_table(table_name, schema)


def construct_tables_with_items(tables):
    """
    db_tables is a dictionary of key: table name, and value: array of items to insert into database
    """
    # First construct the tables
    construct_tables(tables)
    # Now insert the data
    for table, items in tables.items():
        config.DB_CONN.insert_batch(table, items)



def build_database():
    # Create the DB
    db.create_db()
    db_tables = {}
    for task in config.CONFIG['tasks']:
        if task.get('type') == config.CATEGORICAL:
            new_db_tables = build_categorical_tables(task)
            # save the table names
            task['tables'] = sorted(list(new_db_tables.keys()), key=len)
            db_tables.update(new_db_tables)

    # Now we have a dictionary of db_tables, {tablename: items}
    construct_tables_with_items(db_tables)
    config.DB_CONN.create_table('user_log', schema=config.USER_LOG)
