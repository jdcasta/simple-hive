"""
Base to setup the app for a multithread environment
"""
import logging
import time
from server import parser
from server import config
from server import server
from server.application import app


start = time.time()
parser.parse_file(config.DEFAULT_CONFIG_FILE_NAME)
parser.build_database()
logging.info('Finished parsing configuration file and building database.')
logging.info("Total time elapsed %2.1f seconds", (time.time() - start))
server.create_dynamic_static_paths(config.STATIC_DIRS)
