"""
Main Module for the flask server, defines all the routes
"""
import logging
from os.path import join, abspath, basename, dirname

from flask import (send_from_directory, jsonify, abort,
                   render_template, request)

from flask_httpauth import HTTPBasicAuth

from server import config
from server.application import app  # Flask App
from server.database import db

auth = HTTPBasicAuth()


def decorator(static_dir):
    """
    A decorator that allows me to pass arguments to a function, and arbitrarily rename the function
    This is actually pretty atrocious. The issue stems from Flask wanting a unique function 'name'.
    """
    def pass_static(f):
        def decorated_function(*args, **kwargs):
            return f(*args, static_dir=static_dir, **kwargs)
        # I MUST do this in order for flask to accept this function
        # AssertionError: View function mapping is overwriting an existing endpoint function: decorated_function
        # Error still occurs even if I use @wraps
        decorated_function.__name__ = static_dir['slug']
        return decorated_function
    return pass_static


def send_file(path, static_dir):
    """
    The actual function that sends the images from the static directory
    """
    full_path = join(static_dir['directory'], path)
    directory = dirname(full_path)
    file_name = basename(full_path)
    return send_from_directory(directory, file_name)


def create_dynamic_static_paths(static_dirs):
    """
    This will create routes in a Flask app that can serve multiple static directories.
    Unfortunately I dont know ahead of time where the end user could be serving their files from
    so I just construct these routes dynamically.
    """
    for static_dir in static_dirs:
        send_func = decorator(static_dir)(send_file)
        app.route(static_dir['slug'] + '<path:path>')(send_func)
        # print(static_dir)


@auth.get_password
def get_pw(username):
    """ Simple basic HTTP auth, users stored in memory from config.json file """
    if username in config.CONFIG['users']:
        return config.CONFIG['users'].get(username)
    return None


@app.route('/')
def home_page():
    """ Our home page, unprotected """
    return render_template('home.html')


@app.route('/tasks')
@auth.login_required
def tasks_page():
    """
    Lists the tasks, protected route. Note: we pass the authorization credentials back into page.
    Cant access HTTP auth header any other way
    """
    authorization = request.authorization
    return render_template('task_list.html', auth=authorization, tasks=config.CONFIG['tasks'], config=config.CONFIG)


@app.route('/tasks/<int:task_id>')
@auth.login_required
def task_page(task_id):
    """
    This renders a task for a user, taking into account any user defined widgets for their inputs.
    """
    logging.info('Request data: %r', request.json)
    tasks = config.CONFIG['tasks']
    if task_id >= len(tasks):
        abort(404)
    # Need to get an item from the task (look up data in the data tables)
    authorization = request.authorization
    task = tasks[task_id]
    item = db.fetch_task_item(authorization['username'], task)
    if not item:
        # There are no more task this user can complete
        return render_template('task_complete.html', auth=authorization, task=task, config=config.CONFIG)
    # TODO fix this hogwash and actually create a dict of items that is slimmed down to EXACTLY what you need
    task = dict(list(item.items()) + list(task.items()))
    task['inputs'] = create_dicts(task['inputs'], item['inputs'])
    return render_template('task.html', auth=authorization, task=task, config=config.CONFIG)


@app.route('/api/tasks', methods=['POST'])
@auth.login_required
def update_task():
    """
    Updates the database. Needs the task id, the unique id, the categorical value, and the issue flag
    """
    logging.info('Request data: %r', request.json)
    data = request.json['data']
    # TODO: Make sure the request is properly formed
    task_item = {'taskid': data['taskid'], 'uid': data['uid'],
                 'issue': data['issue'], 'category': data['category']}
    result = db.update_task_item(
        task_item, user=request.authorization['username'])
    if result == 0:
        logging.error('Error updating task!: %r', task_item)
    return jsonify({'success': result})


def create_dicts(array1, array2):
    """
    Creates a new dict that is a combination of two dicts.
    The second dict is copied into the key "data" of the first dict.
    """
    new_list = []
    for item1, item2 in zip(array1, array2):
        copy = dict(item1)
        copy.update(item2)
        new_list.append(copy)
    return new_list
