"""
Parses Configuration File
Constructs Database (if not already created)
Launches the website
"""
import logging
import time
import argparse
from server import parser
from server import config
from server import server
from server.application import app


logging.basicConfig(
    format='[%(levelname)s] %(asctime)-8s %(message)s', level=logging.INFO)


def main(file_name):
    start = time.time()
    parser.parse_file(file_name)
    parser.build_database()
    logging.info('Finished parsing configuration file and building database.')
    logging.info("Total time elapsed %2.1f seconds", (time.time() - start))
    logging.info('Starting up the webserver...')
    # print(config.CONFIG)
    # print(config.STATIC_DIRS)
    server.create_dynamic_static_paths(config.STATIC_DIRS)
    # Launch the webserver
    app.run(threaded=True)


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    # other arguments
    argparser.add_argument('--file', type=str, default=config.DEFAULT_CONFIG_FILE_NAME)
    args = argparser.parse_args()
    main(args.file)
